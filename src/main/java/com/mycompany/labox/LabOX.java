/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.labox;

import java.util.Scanner;


/**
 *
 * @author sarit
 */
public class LabOX {
    
    static char [][] GameBoard = {{'-','-','-'},
                                  {'-','-','-'},
                                  {'-','-','-'}};
    static char currentPlayer ='O'; 
    static String Pos;
    static  int count = 0;

    public static void main(String[] args) {
        printWelcome();
        System.out.println(1 + "|" +2+"|"+3);
        System.out.println(4 + "|" +5+"|"+6);
        System.out.println(7 + "|" +8+"|"+9);
        
        while(true){
            printGameBoard();
            printTurn();
            inputPos();
            
            if(cheakWin()){
                printGameBoard();
                printWin();
                break;
            }
            
            if (cheakDraw()){
                printGameBoard();
                printDraw();
                break;
            }
            swtichPlayer();
        }
     
    }

    private static void printWelcome() {
        System.out.println("Welcome to OX Game");
    }

    private static void printGameBoard() {
        for(int i=0;i<3;i++){
            for(int j = 0;j<3;j++){
                System.out.print(GameBoard[i][j]+" ");
            }
            System .out.println("");
        }
    }

    private static void printTurn() {
        System.out.println("Turn "+ currentPlayer);
    }

    private static void inputPos() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Please input Position :");
        Pos = kb.next();
        switch(Pos){
            case "1":
                GameBoard[0][0] = currentPlayer;
                break;
            case "2":
                GameBoard[0][1] = currentPlayer;
                break;
            case "3":
                GameBoard[0][2] = currentPlayer;
                break;
            case "4":
                GameBoard[1][0] = currentPlayer;
                break;
            case "5":
                GameBoard[1][1] = currentPlayer;
                break;
            case "6":
                GameBoard[1][2] = currentPlayer;
                break;
            case "7":
                GameBoard[2][0] = currentPlayer;
                break;
            case "8":
                GameBoard[2][1] = currentPlayer;
                break;
            case "9":
                GameBoard[2][2] = currentPlayer;
                break;
            default:
                System.out.println("Please input Position now!");
        
        }
        count++;
        
    }

    private static void swtichPlayer() {
        if(currentPlayer == 'O'){
            currentPlayer = 'X';
        }else{
            currentPlayer ='O';
        }
    }

    private static boolean cheakWin() {
        if(cheakRow()){
            return true;
        }else if(cheakCol()){
            return true;
        }else if(cheakC1()){
            return true;
        }else if(cheakC2()){
            return true;
        }
        return false;
    }

    private static void printWin() {
        System.out.println(currentPlayer + " wins");
    }

    private static boolean cheakRow() {
        if((GameBoard[0][0] == currentPlayer && GameBoard[0][1] == currentPlayer && GameBoard[0][2] == currentPlayer)||
            (GameBoard[1][0] == currentPlayer && GameBoard[1][1] == currentPlayer && GameBoard[1][2] == currentPlayer)||
            (GameBoard[2][0] == currentPlayer && GameBoard[2][1] == currentPlayer && GameBoard[2][2] == currentPlayer)){
            return true;
        }
        return false;
    }

    private static boolean cheakCol() {
        if((GameBoard[0][0] == currentPlayer && GameBoard[1][0] == currentPlayer && GameBoard[2][0] == currentPlayer)||
            (GameBoard[0][1] == currentPlayer && GameBoard[1][1] == currentPlayer && GameBoard[2][1] == currentPlayer)||
            (GameBoard[0][2] == currentPlayer && GameBoard[1][2] == currentPlayer && GameBoard[2][2] == currentPlayer)){
            return true;
        }
        return false;
    }

    private static boolean cheakC1() {
        if(GameBoard[0][0] == currentPlayer && GameBoard[1][1] == currentPlayer && GameBoard[2][2] == currentPlayer){
            return true;
        }
        return false;
    }

    private static boolean cheakC2() {
        if (GameBoard[0][2] == currentPlayer && GameBoard[1][1] == currentPlayer && GameBoard[2][0] == currentPlayer){
            return true;
        }
        return false;
    }

    private static boolean cheakDraw() {
        if(count==9){
            return true;
        }
        return false;
    }

    private static void printDraw() {
        System.out.println("GameOver");
    }
}
